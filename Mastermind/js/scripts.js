const colors = [];
const playerpions = document.getElementsByClassName("choices");
const playerpegs = Array.from(playerpions);
const computerpions = document.getElementsByClassName("computer");
const computerpegs = Array.from(computerpions);
let computercolors = [];
let dragged;
const computerhints = document.getElementsByClassName("hint");
const displayhints = Array.from(computerhints);
document.getElementById("submit").onclick = function (){checkColors();};
let attempts = 0;





// Making the random colors array

for (let i = 0; colors.length < 4; i++){
	colors.push('#'+ (Math.random().toString(16) + "000000").substring(2,8));
}



// Shuffle the random colors array

function shuffle(array) {
    var m = array.length, t, i;
    array = array.slice(0);
    // While there remain elements to shuffle
    while (m) {
        // Pick a remaining element…
        i = Math.floor(Math.random() * m--);
        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }
    return array;
}
let playercolors = shuffle(colors);

// Assign background color from playercolors array to element

	for (let i=0; i < playercolors.length; i++){
		playerpegs.forEach(playerpeg => {
			playerpeg.style.backgroundColor = playercolors[i++];
		});
		
	}
	
// Make elements draggable
	
	
	function handleDragStart(e) {
	    this.style.opacity = '0.4';
	    dragged = this;

	    e.dataTransfer.effectAllowed = 'copy';
	    e.dataTransfer.setData('text/css', this.style.backgroundColor);
	    
	  }

	  function handleDragEnd(e) {
		  
		  if(this.style.backgroundColor === dragged.style.backgroundColor){
			  this.style.opacity = "";
			    
			    playerpegs.forEach(function (playerpeg) {
			        playerpeg.classList.remove('over');
			      });
			    
			  }else{
				  e.preventDefault();
			  }
	  
			  

	  }
	  function handleDragOver(e) {
		 
		 
		 
		if(this.style.backgroundColor === "rgb(211, 211, 211)" && this.style.backgroundColor != dragged.style.backgroundColor){
			 e.preventDefault();
			 e.dataTransfer.dropEffect = 'copy';
			 
			 

		     
		  }else if(this.style.backgroundColor != "rgb(211, 211, 211)"){
			  
			 
			  this.style.backgroundColor = this.style.backgroundColor;
			  e.returnValue = true;
		  }
		  
	
	  }

	function handleDragEnter(e) {
		
		
		
		  }

	function handleDragLeave(e) {
		if(this.style.backgroundColor != ""){
		this.style.backgroundColor = this.style.backgroundColor;
		}else{
			this.style.backgroundColor = "";
		}
		  }
	
	function handleDrop(e) {
		 
		this.style.backgroundColor = dragged.style.backgroundColor;
		  if (this.style.backgroundColor === dragged.style.backgroundColor) {
			  e.preventDefault();
		    return false;
		   
		  }
		
		  
		}

	  
	  playerpegs.forEach(function(playerpeg) {
		    playerpeg.addEventListener('dragstart', handleDragStart);
		    playerpeg.addEventListener('dragend', handleDragEnd);
		    
		    
		    
		  });
	  computerpegs.forEach(function(computerpeg){
		  
		   computerpeg.addEventListener('dragenter', handleDragEnter);
		   computerpeg.addEventListener('dragover', handleDragOver);
		    computerpeg.addEventListener('dragleave', handleDragLeave);
		    computerpeg.addEventListener('drop', handleDrop);
		    
		
	 });
		
	
// rgb to hex
	  function rgb2hex(rgb) {
		    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		    function hex(x) {
		        return ("0" + parseInt(x).toString(16)).slice(-2);
		    }
		    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
		}
	  

	  

// Check function
	  
	function checkColors() {
		if (attempts < 12){
		// Array compare function
		  
		// attach the .equals method to Array's prototype to call it on any
		// array
		Array.prototype.equals = function (array) {
		    // if the other array is a falsy value, return
		    if (!array)
		        return false;

		    // compare lengths - can save a lot of time
		    if (this.length != array.length)
		        return false;

		    for (var i = 0, l=this.length; i < l; i++) {
		        // Check if we have nested arrays
		        if (this[i] instanceof Array && array[i] instanceof Array) {
		            // recurse into the nested arrays
		            if (!this[i].equals(array[i]))
		                return false;       
		        }           
		        else if (this[i] != array[i]) { 
		            // Warning - two different object instances will never be
					// equal: {x:20} != {x:20}
		            return false;   
		        }           
		    }       
		    return true;
		}
		
		for(i=0; i < 4; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
		
	         if(computercolors.equals(colors) === false){
	        
	        
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[0].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[0].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[1].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[1].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[2].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[2].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[3].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[3].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[0].style.backgroundColor = "red"; 
	        	 displayhints[1].style.backgroundColor = "red"; 
        		 displayhints[2].style.backgroundColor = "red"; 
        		 displayhints[3].style.backgroundColor = "red";
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         }
		
		}
		computercolors = [];
		if(attempts === 1){
		for(i = 4; i < 8; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[4].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[4].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[5].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[5].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[6].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[6].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[7].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[7].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[4].style.backgroundColor = "red"; 
	        	 displayhints[5].style.backgroundColor = "red"; 
        		 displayhints[6].style.backgroundColor = "red"; 
        		 displayhints[7].style.backgroundColor = "red";
        		
        		 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         }
	         
		
		}
		}
		
		computercolors = [];
		if(attempts === 2){
		for(i = 8; i < 12; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[8].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[8].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[9].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[9].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[10].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[10].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[11].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[11].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[8].style.backgroundColor = "red"; 
	        	 displayhints[9].style.backgroundColor = "red"; 
        		 displayhints[10].style.backgroundColor = "red"; 
        		 displayhints[11].style.backgroundColor = "red"; 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         
		
		}
		}
		
		computercolors = [];
		if(attempts === 3){
		for(i = 12; i < 16; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[12].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[12].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[13].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[13].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[14].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[14].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[15].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[15].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[12].style.backgroundColor = "red"; 
	        	 displayhints[13].style.backgroundColor = "red"; 
        		 displayhints[14].style.backgroundColor = "red"; 
        		 displayhints[15].style.backgroundColor = "red"; 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         }
	         
		
		}
		}
		
		computercolors = [];
		if(attempts === 4){
		for(i = 16; i < 20; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[16].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[16].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[17].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[17].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[18].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[18].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[19].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[19].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[16].style.backgroundColor = "red"; 
	        	 displayhints[17].style.backgroundColor = "red"; 
        		 displayhints[18].style.backgroundColor = "red"; 
        		 displayhints[19].style.backgroundColor = "red"; 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         
		
			}
		}
		}
		
		computercolors = [];
		if(attempts === 5){
		for(i = 20; i < 24; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[20].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[20].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[21].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[21].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[22].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[22].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[23].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[23].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[20].style.backgroundColor = "red"; 
	        	 displayhints[21].style.backgroundColor = "red"; 
        		 displayhints[22].style.backgroundColor = "red"; 
        		 displayhints[23].style.backgroundColor = "red"; 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         
		
			}
		}
		}
		
		computercolors = [];
		if(attempts === 6){
		for(i = 24; i < 28; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[24].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[24].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[25].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[25].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[26].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[26].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[27].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[27].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[24].style.backgroundColor = "red"; 
	        	 displayhints[25].style.backgroundColor = "red"; 
        		 displayhints[26].style.backgroundColor = "red"; 
        		 displayhints[27].style.backgroundColor = "red"; 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         
		
			}
		}
		}
		
		computercolors = [];
		if(attempts === 7){
		for(i = 28; i < 32; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[28].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[28].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[29].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[29].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[30].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[30].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[31].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[31].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[28].style.backgroundColor = "red"; 
	        	 displayhints[29].style.backgroundColor = "red"; 
        		 displayhints[30].style.backgroundColor = "red"; 
        		 displayhints[31].style.backgroundColor = "red"; 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         
		
			}
		}
		}
		
		computercolors = [];
		if(attempts === 8){
		for(i = 32; i < 36; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[32].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[32].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[33].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[33].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[34].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[34].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[35].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[35].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[32].style.backgroundColor = "red"; 
	        	 displayhints[33].style.backgroundColor = "red"; 
        		 displayhints[34].style.backgroundColor = "red"; 
        		 displayhints[35].style.backgroundColor = "red"; 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         
		
			}
		}
		}
		
		computercolors = [];
		if(attempts === 9){
		for(i = 36; i < 40; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[36].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[36].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[37].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[37].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[38].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[38].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[39].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[39].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[36].style.backgroundColor = "red"; 
	        	 displayhints[37].style.backgroundColor = "red"; 
        		 displayhints[38].style.backgroundColor = "red"; 
        		 displayhints[39].style.backgroundColor = "red"; 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         
		
			}
		}
		}
		
		computercolors = [];
		if(attempts === 10){
		for(i = 40; i < 44; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[40].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[40].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[41].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[41].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[42].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[42].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[43].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[43].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[40].style.backgroundColor = "red"; 
	        	 displayhints[41].style.backgroundColor = "red"; 
        		 displayhints[42].style.backgroundColor = "red"; 
        		 displayhints[43].style.backgroundColor = "red"; 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         
		
			}
		}
		}
		
		computercolors = [];
		if(attempts === 11){
		for(i = 44; i < 48; i++){
			colorPc = rgb2hex(computerpegs[i].style.backgroundColor);
			computercolors.push(colorPc);
			
			 if(computercolors.equals(colors) === false){
				
	        	 if (computercolors[0] === colors[0]){
	        		 displayhints[44].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[44].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[1] === colors[1]){
	        		 displayhints[45].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[45].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[2] === colors[2]){
	        		 displayhints[46].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[46].style.backgroundColor = "white";
	        	 }
	        	 if (computercolors[3] === colors[3]){
	        		 displayhints[47].style.backgroundColor = "red"; 
	        	 } else{
	        		 displayhints[47].style.backgroundColor = "white";
	        	 }
	         }else{
	        	 displayhints[44].style.backgroundColor = "red"; 
	        	 displayhints[45].style.backgroundColor = "red"; 
        		 displayhints[46].style.backgroundColor = "red"; 
        		 displayhints[47].style.backgroundColor = "red"; 
        		 document.getElementById("feedback").innerHTML = "You won! Click reload to play again. <br />" + '<button value="reload" id="btn-reload" onClick = "refreshPage();">Reload</button>';        		
	         
		
			}
		}
		
         
	}
		
		attempts++
		
			}
		}
}
		
function refreshPage(){
	 window.location.reload();
		} 
	



