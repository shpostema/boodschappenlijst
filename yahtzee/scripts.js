document.getElementById("btn").onclick = function (){throwDices();};
const result = document.getElementsByClassName("dice");
const diceCodes = ['&#x2680;','&#x2681;', '&#x2682;', '&#x2683;', '&#x2684;', '&#x2685;'];
const dices = [1, 2, 3, 4, 5, 6];
let thrownDice = [];
let rolls = 0;
document.getElementById("btnFill").onclick = function (){lockedValue(); fillScores();};
const scores = document.getElementsByClassName("keepScore");
const uppervalues = document.getElementsByClassName("upper");
const upperscores = Array.from(uppervalues);
const cells = Array.from(scores);
const one = document.getElementById("aces1");
const two = document.getElementById("twos1");
const three = document.getElementById("threes1");
const four = document.getElementById("fours1");
const five = document.getElementById("fives1");
const six = document.getElementById("sixes1");
const threeofakind = document.getElementById("threeOfKind1");
const fourofakind = document.getElementById("fourOfKind1");
const yahtzee = document.getElementById("yahtzee1");
const fullhouse = document.getElementById("fullHouse1");
const chance = document.getElementById("chance1");
const smallStraight = document.getElementById("smStraight1");
const largeStraight = document.getElementById("lgStraight1");
const totalAbove = document.getElementById("upperScore1");
const totalAboveAll = document.getElementById("totalUpperScore1");
const bonusPoints = document.getElementById("bonus1");
const upperLower = document.getElementById("upperScoreLower1");
const lowerValues = document.getElementsByClassName("lowerScores");
const lowerscorings = Array.from(lowerValues);
const lowerScoreTotal = document.getElementById("lowerScore1");
const grand = document.getElementById("grandTotal1");
let lockedScores = [{one: null, two: null, three: null, four: null, five: null, six: null, bonus: 35}, {threeofakind: null, fourofakind: null, yahtzee: 50, fullhouse: 25, chance: null, smallstraight: 30, largestraight: 40}];



function throwDices () {

	if (rolls < 3){
		if (thrownDice.length === 0){
		
		for (let i = 0; thrownDice.length < 5; i++){
			
			thrownDice.push({value: Math.floor(Math.random() * 6) +1, locked: false});
			
		}
		for (let i = 0; i < thrownDice.length; i++){
			result[i].innerHTML = diceCodes[thrownDice[i].value -1];
			const diceOutput = dices[thrownDice[i].value -1];
			
				
			
		}
			
		}else {
			for(let i = 0; i < thrownDice.length; i++){
				if (thrownDice[i].locked === false){
					thrownDice[i].value = Math.floor(Math.random() * 6) +1;
					result[i].innerHTML = diceCodes[thrownDice[i].value -1];
					const diceOutput = dices[thrownDice[i].value -1];
					
					
				}
			}
		}
		for(let i = 0; i < result.length; i++){
			result[i].onclick = function() {
				result[i].classList.toggle("diceLocked");
				if (result[i].classList.contains("diceLocked")){
					thrownDice[i].locked = true;
					thrownDice[i].value = thrownDice[i].value;
				}else {
					thrownDice[i].locked = false;
				}
				
			}
			
		}
		
	}
	
	rolls++;
	fillScores();
	lockScores();
	
	
	
	

	
	
}

function resetRolls(){
	
	rolls = 0;
}

function fillScores(){

	lockScores();
	
	const ones = thrownDice.filter(thrownDie => thrownDie.value === 1).length;
	if(ones > 0 && !one.classList.contains("locked") && !one.classList.contains("tmpLocked")){
		one.innerHTML = ones * 1;
	}else if(one.classList.contains("locked") || one.classList.contains("tmpLocked")){
		  lockedScores[0].one = parseInt(one.innerHTML);
			if (lockedScores[0].one === null){
				one.innerHTML = "";
			}else{
				one.innerHTML = lockedScores[0].one;
			}
			
			
		}else {
			one.innerHTML = "";
		} 
		
	
	const twos = thrownDice.filter(thrownDie => thrownDie.value === 2).length;
	if(twos > 0 && !two.classList.contains("locked")){
		two.innerHTML = twos * 2;
	}else if(two.classList.contains("locked")){
		  lockedScores[0].two =  parseInt(two.innerHTML);
		if (lockedScores[0].two === null){
			two.innerHTML = "";
		}else{
			two.innerHTML = lockedScores[0].two;
		}
		
		
	}else {
		two.innerHTML = "";
	} 
	
	const threes = thrownDice.filter(thrownDie => thrownDie.value === 3).length;
	if(threes > 0 && !three.classList.contains("locked")){
		three.innerHTML = threes * 3;
	}else if(three.classList.contains("locked")){
		  lockedScores[0].three =  parseInt(three.innerHTML);
			if (lockedScores[0].three === null){
				three.innerHTML = "";
			}else{
				three.innerHTML = lockedScores[0].three;
			}
			
			
		}else {
			three.innerHTML = "";
		} 
		
	
	const fours = thrownDice.filter(thrownDie => thrownDie.value === 4).length;
	if(fours > 0 && !four.classList.contains("locked")){
		four.innerHTML = fours * 4;
	}else if(four.classList.contains("locked")){
		  lockedScores[0].four =  parseInt(four.innerHTML);
			if (lockedScores[0].four === null){
				four.innerHTML = "";
			}else{
				four.innerHTML = lockedScores[0].four;
			}
			
			
		}else {
			four.innerHTML = "";
		} 
		
	
	const fives = thrownDice.filter(thrownDie => thrownDie.value === 5).length;
	if(fives > 0 && !five.classList.contains("locked")){
		five.innerHTML = fives * 5;
	}else if(five.classList.contains("locked")){
		  lockedScores[0].five =  parseInt(five.innerHTML);
			if (lockedScores[0].five === null){
				five.innerHTML = "";
			}else{
				five.innerHTML = lockedScores[0].five;
			}
			
			
		}else {
			five.innerHTML = "";
		} 
		
	
	const sixes = thrownDice.filter(thrownDie => thrownDie.value === 6).length;
	if(sixes > 0 && !six.classList.contains("locked")){
		six.innerHTML = sixes * 6;
	}else if(six.classList.contains("locked")){
		lockedScores[0].six =  parseInt(six.innerHTML);
		if (lockedScores[0].six === null){
			six.innerHTML = "";
		}else{
			six.innerHTML = lockedScores[0].six;
		}
		
		
	}else {
		six.innerHTML = "";
	} 
	

		
	
		const diceValue = thrownDice.map(function (obj){
			return obj.value;
		});
		
		const sumcounts = diceValue.reduce((a, b) => a + b);
		const counts = {};
		diceValue.forEach(function(i) { counts[i] = (counts[i]||0) + 1;});
		let values = Object.values(counts);
		let threekind =  values.filter(value => value >= 3).length;
		let fourkind =  values.filter(value => value >= 4).length;
		let fivekind =  values.filter(value => value === 5).length;
		let fullhouses =  values.filter(value => value).length;
		
			if(threekind > 0  && !threeofakind.classList.contains("locked")){
				threeofakind.innerHTML = sumcounts;
				
			}else if(threeofakind.classList.contains("locked")){
				lockedScores[1].threeofakind =  threeofakind.innerHTML;
				if (lockedScores[1].threeofakind === null){
					threeofakind.innerHTML = "";
				}else{
					threeofakind.innerHTML = lockedScores[1].threeofakind;
				}
			}else{
				lockedScores[1].threeofakind === null
				threeofakind.innerHTML = "";
			}
			
			if(fourkind > 0  && !fourofakind.classList.contains("locked")){
				fourofakind.innerHTML = sumcounts;
				
			}else if(fourofakind.classList.contains("locked")){
				lockedScores[1].fourofakind =  fourofakind.innerHTML;
				if (lockedScores[1].fourofakind === null){
					fourofakind.innerHTML = "";
				}else{
					fourofakind.innerHTML = lockedScores[1].fourofakind;
				}
			}else{
				lockedScores[1].fourofakind === null
				fourofakind.innerHTML = "";
			}
			
			if(fivekind > 0  && !yahtzee.classList.contains("locked")){
				yahtzee.innerHTML = lockedScores[1].yahtzee;
				
			}else if(yahtzee.classList.contains("locked")){
				lockedScores[1].yahtzee =  yahtzee.innerHTML;
				if (lockedScores[1].yahtzee === null){
					yahtzee.innerHTML = "";
				}else{
					yahtzee.innerHTML = lockedScores[1].yahtzee;
				}
			}else{
				
				yahtzee.innerHTML = "";
			}
			
			if(fullhouses === 2 && values.includes(3)  && !fullhouse.classList.contains("locked")){
				fullhouse.innerHTML = lockedScores[1].fullhouse;
				
			}else if(fullhouse.classList.contains("locked")){
				lockedScores[1].fullhouse =  fullhouse.innerHTML;
				if (lockedScores[1].fullhouse === null){
					fullhouse.innerHTML = "";
				}else{
					fullhouse.innerHTML = lockedScores[1].fullhouse;
				}
			}else{
				
				fullhouse.innerHTML = "";
			}
			
			if(!chance.classList.contains("locked")){
				chance.innerHTML = sumcounts;
				
			}else if(chance.classList.contains("locked")){
				lockedScores[1].chance =  chance.innerHTML;
				if (lockedScores[1].chance === null){
					chance.innerHTML = "";
				}else{
					chance.innerHTML = lockedScores[1].chance;
				}
			}else{
				
				chance.innerHTML = "";
			}
			
			
		const straight = diceValue.sort();
		if (/1234|2345|3456/.test(straight.join("").replace(/(.)\1/,"$1")) && !smallStraight.classList.contains("locked")){
			
				smallStraight.innerHTML = lockedScores[1].smallstraight;
				
			}else if(smallStraight.classList.contains("locked")){
				lockedScores[1].smallstraight =  smallStraight.innerHTML;
					smallStraight.innerHTML = lockedScores[1].smallstraight;
				
			}else{
				
				smallStraight.innerHTML = "";
			}
		
		if (/12345|23456/.test(straight.join("").replace(/(.)\1/,"$1")) && !largeStraight.classList.contains("locked")){
			
			largeStraight.innerHTML = lockedScores[1].largestraight;
			
		}else if(largeStraight.classList.contains("locked")){
			lockedScores[1].largestraight =  largeStraight.innerHTML;
				largeStraight.innerHTML = lockedScores[1].largestraight;
		
		}else{
			
			largeStraight.innerHTML = "";
		}
	
		 const upper = [];
			upperscores.forEach(upperscore => {
				if(upperscore.classList.contains("tmpLocked") || upperscore.classList.contains("locked")){
					upper.push(parseInt(upperscore.innerHTML));
				}
			});
		  const totalupperscore = upper.reduce((a, b) => a + b, 0);
		  
	  if(totalupperscore < 63 && totalupperscore > 0){
		  totalAbove.innerHTML = totalupperscore;
		  totalAboveAll.innerHTML = totalupperscore;
		  upperLower.innerHTML = totalAboveAll.innerHTML;
	  }else if(totalupperscore >= 63){
		  totalAbove.innerHTML = totalupperscore;
		  totalAboveAll.innerHTML = totalupperscore + lockedScores[0].bonus;
		  bonusPoints.innerHTML = lockedScores[0].bonus;
		  upperLower.innerHTML = totalAboveAll.innerHTML;
	  }else{
		  totalAbove.innerHTML = "";
		  totalAboveAll.innerHTML = "";
		  bonusPoints.innerHTML = "";
		  upperLower.innerHTML = "";
	  }
	  
	  const lower = [];
		lowerscorings.forEach(lowerscoring => {
			if(lowerscoring.classList.contains("tmpLocked") || lowerscoring.classList.contains("locked")){
				lower.push(parseInt(lowerscoring.innerHTML));
			}
		});
	  const totallowerscore = lower.reduce((a, b) => a + b, 0);
	
	  if(totallowerscore > 0){
		  lowerScoreTotal.innerHTML = totallowerscore;
		 
		  upperLower.innerHTML = totalAboveAll.innerHTML;
		  grand.innerHTML = totallowerscore + parseInt(upperLower.innerHTML);
		  
	  }else{
		  lowerScoreTotal.innerHTML = "";
		  grand.innerHTML = "";
	  }

	
}


function lockScores(){
	cells.forEach(cell => cell.onclick = (e) => {
		cells.forEach(cell => cell.classList.remove('tmpLocked'));
	  e.target.classList.toggle('tmpLocked');
	  const upper = [];
		upperscores.forEach(upperscore => {
			if(upperscore.classList.contains("tmpLocked") || upperscore.classList.contains("locked")){
				upper.push(parseInt(upperscore.innerHTML));
			}
		});
	  const totalupperscore = upper.reduce((a, b) => a + b, 0);
	  
	  const lower = [];
		lowerscorings.forEach(lowerscoring => {
			if(lowerscoring.classList.contains("tmpLocked") || lowerscoring.classList.contains("locked")){
				lower.push(parseInt(lowerscoring.innerHTML));
			}
		});
	  const totallowerscore = lower.reduce((a, b) => a + b, 0);
	  
	});
	
	
	
	
	
}

function lockedValue() {
	for (let i = 0; i < scores.length; i++){
		  
			if(scores[i].classList.contains("tmpLocked")){
			
				scores[i].classList.remove("tmpLocked");
				scores[i].classList.add("locked");
				const upper = [];
				upperscores.forEach(upperscore => {
					if(upperscore.classList.contains("tmpLocked") || upperscore.classList.contains("locked")){
						upper.push(parseInt(upperscore.innerHTML));
					}
				});
			  const totalupperscore = upper.reduce((a, b) => a + b, 0);
			  
			  const lower = [];
				lowerscorings.forEach(lowerscoring => {
					if(lowerscoring.classList.contains("tmpLocked") || lowerscoring.classList.contains("locked")){
						lower.push(parseInt(lowerscoring.innerHTML));
					}
				});
			  const totallowerscore = lower.reduce((a, b) => a + b, 0);
				
			}
		
		}
	resetRolls();
	resetDiceLock();
}

function resetDiceLock(){
	for(let i = 0; i < result.length; i++){
		thrownDice[i].locked = false;
		result[i].classList.remove("diceLocked");
	}
}







	
