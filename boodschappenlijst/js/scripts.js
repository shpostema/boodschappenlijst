const inputs = document.querySelector('table');
const product = document.getElementsByClassName('productCost');
const productcounts = document.getElementsByClassName('productQuantity');
const subtotal = document.getElementsByClassName('productTotalCost');
const cost = document.getElementById("totalCost");

inputs.addEventListener('change', (event) => {
	console.log('aantal producten is gewijzigd');
	let totalcost = 0;
	for (let i = 0; i < product.length; i++){
		const sumvalue = productcounts[i].value;
		const productvalue = parseFloat(product[i].innerHTML);
		let sum = productvalue * sumvalue;
		subtotal[i].innerHTML = sum.toFixed(2);
		let sub = parseFloat(subtotal[i].innerHTML);
		totalcost = totalcost + sub;

		cost.innerHTML = totalcost.toFixed(2);
		
		
	}
	
	
});