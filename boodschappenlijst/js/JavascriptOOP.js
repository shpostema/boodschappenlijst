class Shape {
	constructor(name, color){
		this.name = name;
		this.color = color;
	}
	
	returnName(){
		return this.name;
	}
	
	retrunColor(){
		return this.color;
	}
}
class Circle extends Shape {
	constructor(radius, color, name){
		super(name, color);
		this.radius = radius;
		
	};
	
	showRadius() {
		return this.radius;
	};
	
	circleArea(){
		return Math.PI * this.radius * this.radius;
	};
};
let circle1 = new Circle(1.5, "blue", "circle1");
console.log(circle1.circleArea());
let circle2 = new Circle(10, "red", "circle2");
console.log(circle2.circleArea());
let circle3 = new Circle(20, "yellow", "circle3");
console.log(circle3.circleArea());

class Square extends Shape {
	constructor(size, name, color){
		super(name, color);
		this.size = size;
	
	};
	
	returnSize() {
		return this.size;
	}
	
	returnArea() {
		return this.size * this.size;
	}
};
let square1 = new Square(2, "square1", "black");
console.log(square1.returnArea());
let square2 = new Square(5, "square2", "green");
console.log(square2.returnArea());
let square3 = new Square(100, "square3", "orange");
console.log(square3.returnArea());

class Rectangle extends Shape {
	constructor(width, height, name, color){
		super(name, color);
		this.width = width;
		this.height = height;
		
	}
	
	returnWidth() {
		return this.width;
	}
	
	returnHeight() {
		return this.height;
	}
	
	returnArea() {
		return this.width * this.height;
	}
}
let rectangle1 = new Rectangle(2, 5, "rectangle1", "black");
console.log(rectangle1.returnArea());
let rectangle2 = new Rectangle(5, 10, "rectangle2", "green");
console.log(rectangle2.returnArea());
let rectangle3 = new Rectangle(100, 100, "rectangle3", "orange");
console.log(rectangle3.returnArea());
let circle4 = new Circle(50, "blue", "circle4");
console.log(circle4.returnName());
