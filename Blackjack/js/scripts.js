

function deck(){
	
	this.names = ['A','2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];
	this.suits = ['Hearts','Diamonds','Spades','Clubs'];
	let cards = [];
	this.cardsDrawn = 0;
	this.values = [[1, 11], 2, 3 ,4 ,5 , 6, 7, 8, 9, 10, 10, 10, 10];
	  
	
	
    
    for( let s = 0; s < this.suits.length; s++ ) {
        for( let n = 0; n < this.names.length; n++ ) {
            cards.push( new Card(this.values[n],this.names[n], this.suits[s] ) );
        }
    }

    return cards;
}

function Card(value, name, suit){
	this.value = value;
	this.name = name;
	this.suit = suit;
}


 
let myDeck = new deck();


function shuffle(o) {
	for(let j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
};

myDeck = shuffle(myDeck);

console.log(myDeck);